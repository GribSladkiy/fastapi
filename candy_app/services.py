import functools

import bcrypt
from starlette import status
from starlette.responses import Response


def hash_password(password: str):
    return bcrypt.hashpw(password.encode('utf-8'), bcrypt.gensalt()).decode('utf-8')


def verify_password(input_password, password):
    return bcrypt.checkpw(input_password.encode('utf-8'), password.encode('utf-8'))


def login_required(func):
    @functools.wraps(func)
    def decorate_func(*args, **kwargs):
        if kwargs['request'].cookies.get('AuthToken', None):
            return func(*args, **kwargs)
        return Response(status_code=status.HTTP_403_FORBIDDEN)
    return decorate_func
