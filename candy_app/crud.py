from sqlalchemy.orm import Session

from . import models, schemas
from .services import hash_password, verify_password


def get_couriers(courier_type: str, db: Session, limit: int = 10):
    if courier_type:
        return db.query(models.Courier).filter(models.Courier.type == courier_type).limit(limit).all()
    return db.query(models.Courier).limit(limit).all()


def create_courier(db: Session, courier: schemas.CourierCreate):
    db_courier = models.Courier(**courier.dict())
    db.add(db_courier)
    db.commit()
    return courier


def update_courier(db: Session, courier: schemas.CourierCreate, pk: int):
    db_courier = db.query(models.Courier).filter(models.Courier.id == pk).first()
    if db_courier is None:
        db_courier = models.Courier(**courier.dict())
        db.add(db_courier)
        db.commit()
    else:
        db.query(models.Courier).filter(models.Courier.id == pk).update(courier.dict())
        db.commit()
        db.refresh(db_courier)
    return db_courier


def get_courier_by_id(db: Session, pk: int):
    db_courier = db.query(models.Courier).filter(models.Courier.id == pk).first()
    return db_courier


def delete_courier_by_id(db: Session, pk: int):
    db_courier = db.query(models.Courier).filter(models.Courier.id == pk).delete()
    db.commit()
    return db_courier


def user_exist(db: Session, user: schemas.UserCreate):
    db_user = db.query(models.User).filter(models.User.username == user.username).first()
    if db_user is not None:
        return True
    return False


def create_user(db: Session, user: schemas.UserCreate):
    user.password = hash_password(user.password)
    db.add(models.User(**user.dict()))
    db.commit()


def check_user(db: Session, user: schemas.User):
    db_user = db.query(models.User).filter(models.User.username == user.username).first()
    return db_user and verify_password(user.password, db_user.password)
