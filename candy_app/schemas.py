from pydantic import BaseModel


class SessionData(BaseModel):
    username: str


class CourierCreate(BaseModel):
    type: str
    region: list[int]
    time: list[str]


class Courier(CourierCreate):
    id: int

    class Config:
        orm_mode = True


class User(BaseModel):
    username: str
    password: str


class UserCreate(User):
    email: str

    class Config:
        orm_mode = True
