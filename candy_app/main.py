from typing import List, Optional

from fastapi import FastAPI, Depends
from sqlalchemy.orm import Session
from starlette import status
from starlette.requests import Request
from starlette.responses import RedirectResponse
from starlette.responses import Response
from fastapi_sessions.backends import InMemoryBackend
from fastapi_sessions import SessionCookie, SessionInfo

from . import crud, schemas
from . import models
from .database import SessionLocal, engine
from .services import login_required

models.Base.metadata.create_all(bind=engine)

test_session = SessionCookie(
    name="session",
    secret_key="helloworld",
    backend=InMemoryBackend,
    data_model=schemas.SessionData,
    scheme_name="Test Cookies",
    auto_error=False
)

app = FastAPI()


# Dependency
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.post('/courier', response_model=schemas.CourierCreate)
def create_courier(courier: schemas.CourierCreate, db: Session = Depends(get_db)):
    return crud.create_courier(db, courier)


@app.get('/courier', response_model=List[schemas.Courier])
@login_required
def get_all_couriers(request: Request, courier_type: str = '', db: Session = Depends(get_db), limit: int = 10):
    print(request.cookies)
    return crud.get_couriers(courier_type, db, limit)


@app.get('/courier/{pk}', response_model=schemas.Courier)
def get_courier_by_id(pk: int, db: Session = Depends(get_db)):
    return crud.get_courier_by_id(db, pk)


@app.put('/courier/{pk}', response_model=schemas.Courier)
def update_courier(pk: int, courier: schemas.CourierCreate, db: Session = Depends(get_db)):
    return crud.update_courier(db, courier, pk)


@app.delete('/courier/{pk}')
def delete_courier(pk: int, db: Session = Depends(get_db)):
    if crud.delete_courier_by_id(db, pk):
        return Response(status_code=status.HTTP_200_OK)
    else:
        return Response(status_code=status.HTTP_400_BAD_REQUEST)


@app.post('/register')
def register_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
    if crud.user_exist(db, user):
        return Response(status_code=status.HTTP_400_BAD_REQUEST)
    crud.create_user(db, user)
    return RedirectResponse(url='/')


@app.post('/login')
def login(request: Request, user: schemas.User, response: Response, session_data: Optional[SessionInfo] = Depends(test_session), db: Session = Depends(get_db)):
    print(user.username, user.password)
    if crud.check_user(db, user):
        # test_user = schemas.SessionData(username=user.username)
        test_session.create_session(user.username, response)
        if session_data is None:
            response_ = Response()
            request.cookies['user'] = user.username
            response_.set_cookie('AuthToken', user.username)
            return response_
    return RedirectResponse(url='/login')
