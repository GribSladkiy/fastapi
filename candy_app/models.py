from sqlalchemy import Boolean, Column, ForeignKey, Integer, String, ARRAY
from sqlalchemy.orm import relationship
from sqlalchemy.dialects import postgresql
from .database import Base
# from sqlalchemy_utils.types import email


class Courier(Base):
    __tablename__ = "Courier"
    id = Column(Integer, primary_key=True, index=True, autoincrement=True)
    type = Column(String, nullable=True)
    region = Column(postgresql.ARRAY(Integer))
    time = Column(postgresql.ARRAY(String))


class User(Base):
    __tablename__ = "User"
    id = Column(Integer, primary_key=True, autoincrement=True)
    username = Column(String)
    password = Column(String)
    email = Column(String)
